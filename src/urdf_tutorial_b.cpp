#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>

double deltaPan;
double deltaTilt;
double scale;
const double degree2rad = M_PI / 180;
geometry_msgs::Twist joint_state_with_teleop;
double pan = 0.0;
double tilt = 0.0;

// change_scale_callback function for change_scale service
bool change_scale_callback(urdf_tutorial::changescale::Request& req, urdf_tutorial::changescale::Response& resp)
{
    scale = req.s;
    return true;
}

// teleop_callback to update control signal from the keyboard
void teleop_callback(const geometry_msgs::Twist& teleop_msg)
{
    joint_state_with_teleop.linear.x = teleop_msg.linear.x;    // equal 2 when press up, -2 when down
    joint_state_with_teleop.angular.z = teleop_msg.angular.z;  // 2 for leftkey, -2 for rightkey

    // moving one degree
    deltaPan = degree2rad * scale * joint_state_with_teleop.linear.x / 2;
    deltaTilt = degree2rad * scale * joint_state_with_teleop.angular.z / 2;

    pan = pan + deltaPan;
    tilt = tilt + deltaTilt;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "urdf_tutorial");
    ros::NodeHandle n;

    // The change_scale server object
    ros::ServiceServer server = n.advertiseService("urdf_tutorial/change_scale", &change_scale_callback);

    // The node advertises the joint values of the pan-tilt
    ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);

    // Create subscriber object
    ros::Subscriber sub = n.subscribe("teleop_values", 1, &teleop_callback);

    ros::Rate loop_rate(30);

    // message declarations
    sensor_msgs::JointState joint_state;
    joint_state.name.resize(2);
    joint_state.position.resize(2);

    deltaPan = 0.0;
    deltaTilt = 0.0;
    scale = 0.5;
    joint_state_with_teleop.linear.x = 0.0;
    joint_state_with_teleop.angular.z = 0.0;

    while (ros::ok()) {
        // update joint_state
        joint_state.header.stamp = ros::Time::now();
        joint_state.name[0] = "pan_joint";
        joint_state.position[0] = pan;
        joint_state.name[1] = "tilt_joint";
        joint_state.position[1] = tilt;

        // send the joint state
        joint_pub.publish(joint_state);

        // check the callback when subscribe
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

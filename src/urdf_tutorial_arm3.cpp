#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>

double deltaElbow;
double deltaWrist;
double scale;
const double degree2rad = M_PI / 180;
geometry_msgs::Twist joint_state_with_teleop;
double elbow = 0.0;
double wrist = 0.0;

// teleop_callback to update control signal from the keyboard
void teleop_callback(const geometry_msgs::Twist& teleop_msg)
{
    joint_state_with_teleop.linear.x = teleop_msg.linear.x;    // equal 2 when press up, -2 when down
    joint_state_with_teleop.angular.z = teleop_msg.angular.z;  // 2 for leftkey, -2 for rightkey

    // moving one degree
    deltaElbow = degree2rad * scale * joint_state_with_teleop.linear.x / 2;
    deltaWrist = degree2rad * scale * joint_state_with_teleop.angular.z / 2;

    elbow = elbow + deltaElbow;
    wrist = wrist + deltaWrist;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "urdf_tutorial_arm3");
    ros::NodeHandle n;

    // The node advertises the joint values of the elbow-wrist
    ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);

    // Create subscriber object
    ros::Subscriber sub = n.subscribe("teleop_values", 1, &teleop_callback);

    ros::Rate loop_rate(10.0);

    // message declarations
    sensor_msgs::JointState joint_state;
    joint_state.name.resize(9);
    joint_state.position.resize(9);

    deltaElbow = 0.0;
    deltaWrist = 0.0;
    scale = 10.0;
    joint_state_with_teleop.linear.x = 0.0;
    joint_state_with_teleop.angular.z = 0.0;

    // define tf listener
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);

    while (ros::ok()) {
        geometry_msgs::TransformStamped transformStamped;

        // update joint_state
        joint_state.header.stamp = ros::Time::now();
        joint_state.name[0] = "elbow_pitch_joint";
        joint_state.position[0] = elbow;
        joint_state.name[1] = "wrist_pitch_joint";
        joint_state.position[1] = wrist;
        joint_state.name[2] = "shoulder_pan_joint";
        joint_state.position[2] = 0.0;
        joint_state.name[3] = "shoulder_pitch_joint";
        joint_state.position[3] = 0.0;
        joint_state.name[4] = "elbow_roll_joint";
        joint_state.position[4] = 0.0;
        joint_state.name[5] = "wrist_roll_joint";
        joint_state.position[5] = 0.0;
        joint_state.name[6] = "gripper_roll_joint";
        joint_state.position[6] = 0.0;
        joint_state.name[7] = "finger_joint1";
        joint_state.position[7] = 0.0;
        joint_state.name[8] = "finger_joint2";
        joint_state.position[8] = 0.0;

        // send the joint state
        joint_pub.publish(joint_state);

        // check if there are something wrong
        try {
            transformStamped =
                tfBuffer.lookupTransform("base_link", "grasping_frame", ros::Time(0), ros::Duration(3.0));
        }
        catch (tf2::TransformException& ex) {
            ROS_WARN("%s", ex.what());
            ros::Duration(1.0).sleep();
            continue;
        }

        // check if grasping_frame height is lower than 0,3m
        // then reset the scale value to slow down the teleoperation
        // note that the base_link is a box with size 0.1 0.1 0.1
        if (transformStamped.transform.translation.z <= 0.25) {
            scale = 0.5;
        } else {
            scale = 10.0;
        }

        // check the callback when subscribe
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

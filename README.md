Session 6: Tools (https://sir.upc.edu/projects/rostutorials)

Exercise 4a: 
1. Modify the code of the urdf_tutorial_template.cpp file to add a subscriber to a topic called teleop_values in order to teleoperate the pan and tilt degrees of freedom by using the arrow keys, which will be controlled by the turtlesim/teleop_turtle node.
2. Save the code as urdf_tutorial_a.cpp, and name the executable urdf_tutorial_a.

Exercise 4b:
1. Add a service to change the scale (call it urdf_tutorial/change_scale) in order to teleoperate the pan-tilt structure in a more fine or coarse way.
2. Save the code as urdf_tutorial_b.cpp, and name the executable urdf_tutorial_b.

Exercise 4c:
1. Change the pan-tilt structure by the 7-dof robot. Teleoperate joints elbow_pitch_joint and wrist_pitch_joint.
2. Save the code as urdf_tutorial_arm.cpp, and name the executable urdf_tutorial_arm.

Exercise 4e:
Add a TransformListener to monitorize the z value of the grasping_frame and slow down the teleoperation when its value is below 0.3 m.
Save the code as urdf_tutorial_arm3.cpp, and name the executable urdf_tutorial_arm3.
